/**
 * Simple script to detect misc CSS @support's.
 * If not... Redirect to: /upgrade
 *
 * To minify (example):
 *      uglifyjs detect_support.js -o detect_support.min.js -c -m
 */
(function detectSupport() {
    var upradeURL = '/upgrade',

        /**
         * List your CSS @support tests.
         * On the upgrade page, it will add to console.log():
         *      Your browser doesn't support “Foo Bar Variables”: @supports ($foo, Bar)
         */
        cssObj = {
            'CSS Variables': ['--var', 0],
            //'Foo Bar Variables': ['$foo', 'bar'],
        };

    var redirect = (function() {
        window.location = upradeURL;
    });

    for (var t in cssObj) {
        if (window.CSS && window.CSS.supports && !window.CSS.supports(cssObj[t][0], cssObj[t][1])) {
            sessionStorage.setItem('detect_support.js', 'Your browser doesn\'t support “' + t + '”: @supports (' + cssObj[t][0] + ', ' + cssObj[t][1] + ')');
            redirect();
        }
    }
})();

/**
 *  In your upgrade page add:
 *
 *      <script type="text/javascript">
 *      if (sessionStorage.getItem('detect_support.js')) {
 *          console.log(sessionStorage.getItem('detect_support.js'));
 *          sessionStorage.removeItem('detect_support.js');
 *      }
 *      </script>
 */