<?php
/**
 * /upgrade/index.php
 *
 * @author Eric F (iEFdev)
 * @copyright (c) 2014-2020 Eric F
 * @license MIT License (MIT)
 * @description Just a simple (friendly) way to show the visitors they have an outdated and unsupported browser.
 */

$page_vars = [
    'year'  => date('Y'),
    'site'  => $_SERVER['HTTP_HOST'],
    'new'   => 'onclick="window.open(this.href); return false;"',
    'ua'    => $_SERVER['HTTP_USER_AGENT'],
];
extract($page_vars);

$text_vars = [
    'title'         => 'Oops! &middot; Please, upgrade your browser',
    'header'        => 'Your browser is not supported',

    'intro'         => 'How did I get here?',
    'intro_exp'     => 'The web server detected that your web browser is too old, and not compatible with some of the features/technology we use on this site. Please, upgrade to a modern fully supported browser.',

    'browsers'      => 'Browsers',
    'browsers_exp'  => 'Here\'s a list of a few browsers you can use. Some better than others. Only your taste and preferences can decide which one to prefer.',
    'footer'        => sprintf('&copy; 2014-%1s, <a href="//%2s/">%3s</a>', $year, $site, $site),
];
extract($text_vars, EXTR_PREFIX_ALL, 'l');

$browsers = [
    'ff' => [
        'title' => 'Firefox',
        'url'   => 'https://www.mozilla.org/firefox/new/',
    ],
    'chr_oss' => [
        'title' => 'Chromium',
        'url'   => 'https://download-chromium.appspot.com',
    ],
    'chr' => [
        'title' => 'Chrome',
        'url'   => 'https://www.google.com/chrome/',
    ],
    'saf' => [
        'title' => 'Safari',
        'url'   => 'https://www.apple.com/safari/',
    ],
    'op' => [
        'title' => 'Opera',
        'url'   => 'https://www.opera.com/',
    ],
    'edge' => [
        'title' => 'Microsoft Edge',
        'url'   => 'https://www.microsoft.com/microsoft-edge/',
    ],
];

function browserBox(array $array, $target)
{
    foreach ($array as $class => $browser) {
        extract($browser);
        $urlText = preg_replace('/http(s)?:\/\/(www\.)?/', '', $url);
        $box .= <<<BOX
        <div class="box {$class}">
            <h3>{$title}</h3>
            <p><a href="{$url}" {$target} title="{$title}: {$url}">{$urlText}</a></p>
        </div>

BOX;
    }
    return $box;
}

/**
 * Using an older doctype and html for better compability with older
 * browsers, since they're they ones likely to end up here.
 */
?>
<!doctype html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?= $l_title; ?></title>
<link rel="icon" href="/upgrade/img/favicon.png" type="image/png" />
<link rel="stylesheet" href="/upgrade/css/upgrade.css" type="text/css" />
<script type="text/javascript" src="/upgrade/js/upgrade.js"></script>
</head>
<body>
    <div class="wrapper">
        <h1><?= $l_header; ?></h1>
        <h2><?= $l_intro; ?></h2>
        <p><?= $l_intro_exp; ?></p>
        <hr />
        <h2><?= $l_browsers; ?></h2>
        <p><?= $l_browsers_exp; ?></p>
        <hr class="separator" />
<?=
            browserBox($browsers, $new);
?>
        <hr class="separator" />
        <div class="footer">
            <span><?= $l_footer; ?></span>
        </div>
    </div>
    <!-- Placeholder for stats (ie Statcounter, etc) -->
</body>
</html>
