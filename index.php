<?php
/**
 * /upgrade/index.php
 *
 * @author Eric F (iEFdev)
 * @copyright (c) 2014-2020 Eric F
 * @license MIT License (MIT)
 * @description Just a dummy index page ... just to make the example/demo.
 */

$page_vars = [
    'year'  => date('Y'),
    'site'  => $_SERVER['HTTP_HOST'],
    //'ua'    => $_SERVER['HTTP_USER_AGENT'],
];
extract($page_vars);

$text_vars = [
    'title'     => 'Index :: Dummy',
    'header'    => 'Index',
    'intro_exp' => 'Just a dummy page&hellip;</p><p>Change your <q>User Agent</q> to see it work.',
    'footer'    => sprintf('&copy; 2014-%1s, <a href="//%2s/">%3s</a>', $year, $site, $site),
];
extract($text_vars, EXTR_PREFIX_ALL, 'l');

/**
 * Using an older doctype and html for better compability with older
 * browsers, since they're they ones likely to end up here.
 */
?>
<!doctype html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?= $l_title; ?></title>
<link rel="icon" href="/upgrade/img/favicon.png" type="image/png" />
<link rel="stylesheet" href="/upgrade/css/upgrade.css" type="text/css" />

<script type="text/javascript" src="/upgrade/js/detect_support.js"></script>
<!-- //
    “detect_support.js” will check for specific CSS @support, and
    will redirect to /upgrade if the browser can't use it.

    To add more tests, edit the “cssObj” variable in the script/function.
// -->
</head>
<body>
    <div class="wrapper">
        <h1><?= $l_header; ?></h1>
        <p><?= $l_intro_exp; ?></p>
        <hr class="separator" />
        <div class="footer">
            <span><?= $l_footer; ?></span>
        </div>
    </div>
    <!-- Placeholder for stats (ie Statcounter, etc) -->
</body>
</html>
